let mapleader =" "
filetype off
syntax on
set number relativenumber
noswap

call plug#begin('~/.vim/plugged')
Plug 'junegunn/goyo.vim'
Plug 'xuhdev/vim-latex-live-preview', {'for': 'tex'}
Plug 'easymotion/vim-easymotion'
Plug 'itchyny/lightline.vim'
call plug#end()

""""""""""General Settings""""""""""

"---vim-latex-live-preview

" default viewer

let g:livepreview_previewer = 'zathura'

" bitlatex fix

let g:livepreview_use_biber = 1

"---lightline

set laststatus=2 " required

""""""""""Vim Bindings""""""""""

" enable/disable goyo
nnoremap <leader>g :Goyo<CR>

" Exit insert mode
inoremap jk <Esc>

" insert new lines without leaving normal mode
nnoremap <Return> o<esc>

" save

nnoremap <leader>s :w<CR>

"""""""""Window & Tab navigation"""""""""""

" move between splits using hjkl keys

nnoremap <leader>wh <C-w>h
nnoremap <leader>wj <C-w>j
nnoremap <leader>wk <C-w>k
nnoremap <leader>wl <C-w>l

" close split (also works closing stuff in general)

nnoremap <leader>wc :close<CR>

" make vertical, horizontal split

nnoremap <leader>wvs :vs<CR>
nnoremap <leader>ws :split<CR>

" creat tab

nnoremap <leader>tt :tabe<CR>

" next previous tab

nnoremap gl gt
nnoremap gh gT
